"""contest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
# from django.conf import settings
# from django.conf.urls.static import static
from filebrowser.sites import site

from codewar import views

urlpatterns = [
    path(r'admin/tinymce/', include('tinymce.urls')),
    path(r'admin/filebrowser/', site.urls),
    path(r'admin/', admin.site.urls),
    path(r'login', views.LoginView.as_view(), name='login'),
    path(r'logout', views.user_logout, name='logout'),
    path(r'', views.IndexView.as_view(), name='index'),
    path(r'event', views.EventView.as_view(), name='event'),
    path(
        r'event/<int:pk>/',
        views.EventProcessView.as_view(),
        name='event_detail'
    ),
    path(
        r'event/<int:pk>/leave',
        views.leave_event,
        name='event_leave'
    ),
    path(r'register', views.RegisterView.as_view(), name='register'),
    # path(r'forgotpassword', views.forgot_password, name='forgotpassword'),
    # path(
    #     r'passwordresetconfirm/<str:uidb64>/<str:token>',
    #     views.passwordresetconfirm,
    #     name='password-reset'
    # ),
    path(r'codechallenges/', include('codewar.urls')),
]
# + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
