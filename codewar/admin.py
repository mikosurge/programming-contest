from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.http import HttpResponse

import xlwt
import datetime

from codewar.models import (
    Event, Problem, Attempt, TestCase, Score, GroupEvent, UserEvent
)
import codewar.utils as utils
from codewar.enums import EventStatus
from codewar.tasks import generate_testcase

# Register your models here.
@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    actions = ['create_report']
    ordering = ['-time_start']
    list_display = [
        'name', 'prefix', 'time_start', 'time_end', 'is_teamwork',
        'is_leaderboard', 'status', 'type'
    ]
    list_filter = ['type', 'status', 'is_teamwork']
    list_per_page = 50
    search_fields = ['event_name']

    def create_report(self, request, queryset):
        for obj in queryset:
            if not obj.is_teamwork:
                response = HttpResponse(content_type='application/ms-excel')
                response['Content-Disposition'] = \
                    f'attachment; filename="{obj.name}.xls"'
                wb = xlwt.Workbook(encoding='utf-8')
                ws = wb.add_sheet(obj.name)
                rownum = 0
                font_style = xlwt.XFStyle()
                font_style.font.bold = True
                columns = [
                    'Full Name', 'Email', 'Event', 'Score']
                for colnum, val in enumerate(columns):
                    ws.write(rownum, colnum, val, font_style)

                font_style = xlwt.XFStyle()
                rows = utils.get_statistic(obj.pk)
                for row in rows:
                    rownum += 1
                    ws.write(rownum, 0, row.get('fullname'), font_style)
                    ws.write(rownum, 1, row.get('email'), font_style)
                    ws.write(rownum, 2, row.get('event'), font_style)
                    ws.write(rownum, 3, row.get('score'), font_style)
                wb.save(response)
                return response
    create_report.short_description = "Create XLS Report"


@admin.register(Score)
class ScoreAdmin(admin.ModelAdmin):
    list_display = [
        'get_participant_name', 'get_full_name',
        'event', 'score', 'modified_at'
    ]
    list_filtere = ['user', 'event']
    list_per_page = 100


@admin.register(GroupEvent)
class GroupEventAdmin(admin.ModelAdmin):
    list_display = ['event', 'get_groupname', 'created_at', 'modified_at']
    search_fields = ['event']
    save_on_top = True
    list_per_page = 100


@admin.register(UserEvent)
class UserEventAdmin(admin.ModelAdmin):
    list_display = [
        'event', 'get_username', 'is_teamwork',
        'created_at', 'modified_at', 'is_active',
    ]
    search_fields = ['event']
    list_per_page = 100


class TestCaseInline(admin.TabularInline):
    model = TestCase
    extra = 0


@admin.register(Problem)
class ProblemAdmin(admin.ModelAdmin):
    list_display = [
        'title', 'type', 'event', 'category', 'score', 'required_score',
        'max_attempts', 'max_wrong', 'penalty_score', 'published',
        'solution_shown', 'modified_at', 'user',
    ]
    list_filter = [
        'event', 'type', 'published', 'solution_shown', 'category',
    ]
    search_fields = ['title', 'content']
    inlines = [TestCaseInline]
    actions = [
        'create_testcase', 'publish', 'suppress',
        'display_solution', 'hide_solution']
    fieldsets = (
        ('Advanced options', {
            'classes': ('collapse', ),
            'fields': (
                'category', 'score', 'required_score', 'max_attempts',
                'penalty_score', 'max_wrong',
            ),
        }),
        (None, {
            'fields': (
                'event', 'type', 'title', 'content',
                'binary_server', 'solution',
            )
        }),
    )
    list_per_page = 100

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

   # TODO Find which function calls it
    def save_model(self, request, obj, form, change):
        if not change:
            obj.user = request.user
        obj.save()

    def create_testcase(self, request, queryset):
        for obj in queryset:
            # TODO Change to Enum
            if obj.type == 0:
                generate_testcase(obj.id)
    create_testcase.short_description = (
        "Create test cases for selected problems")

    def _triggerlist(self, queryset, attr, value):
        for obj in queryset:
            setattr(obj, attr, value)
            obj.modified = datetime.date.today()
            obj.save()

    def display_solution(self, request, queryset):
        self._triggerlist(queryset, 'solution_shown', True)
    display_solution.short_description = "Show solution for selected problems"

    def hide_solution(self, request, queryset):
        self._triggerlist(queryset, 'solution_shown', False)
    hide_solution.short_description = "Hide solution for selected problems"

    def publish(self, request, queryset):
        self._triggerlist(queryset, 'published', True)
    publish.short_description = "Publish selected problems"

    def suppress(self, request, queryset):
        self._triggerlist(queryset, 'published', False)
    suppress.short_description = "Suppress selected problems"

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'event':
            kwargs['queryset'] = Event.objects.filter(
                status__in = [
                    EventStatus.FINISHED,
                    EventStatus.ACTIVE,
                ]
            )
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


@admin.register(Attempt)
class AttemptAdmin(admin.ModelAdmin):
    list_display = [
        'get_participant_name', 'problem', 'event', 'language',
        'status', 'created_at', 'marked_score'
    ]
    list_filter = [
        'language', 'status', 'event'
    ]
    search_fields = ['problem__title', 'content']
    list_per_page = 100

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return (
            qs if request.user.is_superuser
            else qs.filter(problem__user=request.user)
        )
