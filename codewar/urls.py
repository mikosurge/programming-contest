from django.urls import path
from django.views.decorators.cache import cache_page

from codewar import views


app_name = 'codewar'


urlpatterns = [
    path(r'', views.HomeView.as_view(), name='home'),
    path(r'history/', views.HistoryView.as_view(), name='history'),
    path(r'editor/<int:pk>/', views.CodeEditorView.as_view(), name='editor'),
    path(r'attempt/<int:problem_id>/', views.process_attempt, name='attempt'),
    path(r'howto/', views.HowToView.as_view(), name='howto'),
    path(
        r'topten/',
        cache_page(5)(views.TopTenView.as_view()),
        name='topten'
    ),
    path(r'feedback/', views.feedback, name='feedback'),
    path(r'me/', views.ProfileView.as_view(), name='me'),
    path(r'profile/<int:pk>/', views.ProfileView.as_view(), name='profile'),
    path(
        r'showsolution/<int:problem_id>/',
        views.show_solution,
        name='showsolution'
    ),
]
