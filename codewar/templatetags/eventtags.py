from django import template
import copy


from codewar.models import UserEvent
import codewar.utils as utils
from codewar.enums import EventStatus


register = template.Library()

with open('codewar/templates/literal/card-body.html') as f:
    bodytmpl = f.read()

_propdict = {
    'joined': {
        'header': 'card-header-yellow',
        'border': 'border-warning',
        'button': (
            'btn-warning', 'fa-reply-all', 'Leave', '', '#event/{pk}/leave'
        ),
    },
    'disabled': {
        'header': 'card-header-gray',
        'border': 'border-secondary',
        'button': (
            'btn-secondary', '', '', 'disabled', ''
        ),
        'style': 'opacity: 0.5',
    },
    'active': {
        'header': 'card-header-green',
        'border': 'border-success',
        'button': (
            'btn-success', 'fa-star', 'Join Event', '', '#event/{pk}'
        ),
    },
    'finished': {
        'header': 'card-header-gray',
        'border': 'border-secondary',
        'button': (
            'btn-secondary', 'fa-lightbulb-o', 'Show Result', '', '#event/{pk}'
        ),
    },
    'coming': {
        'header': 'card-header-red',
        'border': 'border-danger',
        'button': (
            'btn-danger', 'fa-magic', 'Register', '', '#event/{pk}'
        ),
    },
}


def disable(prop):
    return {
        **copy.deepcopy(prop),
        **_propdict['disabled'],
        'button': ('btn-secondary', prop['button'][1], prop['button'][2],
        'disabled', prop['button'][4])
    }


_propmap = {
    (EventStatus.ACTIVE, True): _propdict['joined'],
    (EventStatus.ACTIVE, False): disable(_propdict['active']),
    (EventStatus.ACTIVE, None): _propdict['active'],
    (EventStatus.FINISHED, True): _propdict['joined'],
    (EventStatus.FINISHED, False): disable(_propdict['finished']),
    (EventStatus.FINISHED, None): _propdict['finished'],
    (EventStatus.INACTIVE, True): _propdict['joined'],
    (EventStatus.INACTIVE, False): disable(_propdict['coming']),
    (EventStatus.INACTIVE, None): _propdict['coming'],
}


def _get_icon_classes(event):
    return 'fa fa-users' if event.is_teamwork else 'fa fa-user'

def _get_header_class(status, has_joined):
    return _propmap[(status, has_joined)]['header']


def _get_border_class(status, has_joined):
    klass = _propmap[(status, has_joined)].get('border')
    if style := _propmap[(status, has_joined)].get('style'):
        style = f' style="{style}"'
    else:
        style = ''
    return f'"card {klass}"{style}'


def _get_action_button(event, status, has_joined):
    btnprop = _propmap[(status, has_joined)]['button']
    type_, icon, text, status, href = btnprop
    href = href.format(pk=str(event.pk))
    return (
        '<div class="text-center">'
        f'<div id="countdown-{str(event.pk)}" class="countdown"></div>'
        f'<a class="btn {type_} btn-event {status}" href="{href}">'
        f'<i class="fa {icon}"></i> '
        f'{text}</a>'
        '</div>'
    )


def _get_user_button(event, nusers):
    return (
        '<div class="text-center">'
        f'<i class="{_get_icon_classes(event)} fa-lg"></i>'
        f'<p style="margin-bottom: unset;">{str(nusers)} Users</p></div>'
    )


def _get_header(event, status, has_joined):
    return (
        '<div class="card-header text-white '
        f'{_get_header_class(status, has_joined)}">'
        f'<i class="icon-badge fa-lg"></i>'
        f'<b>{str(event.name)}</b></div>'
    )


def _get_body(event, status, has_joined):
    nusers = UserEvent.objects.filter(event=event).all().count()
    return bodytmpl.format(
        description=str(event.description),
        time_start=event.get_time_start(),
        time_end=event.get_time_end(),
        user_button=_get_user_button(event, nusers),
        action_button=_get_action_button(event, status, has_joined)
    )


@register.filter
def get_card(event, user):
    status = event.get_current_status()
    user_event = utils.get_active_event(user)
    has_joined = (
        None if not user_event
        else True if user_event == event
        else False
    )
    border_cls = _get_border_class(status, has_joined)
    header = _get_header(event, status, has_joined)
    body = _get_body(event, status, has_joined)

    return (
        f'<div name="cardEvent" class={border_cls}>'
        f"{header}{body}</div>"
    )

