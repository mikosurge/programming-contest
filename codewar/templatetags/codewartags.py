from django import template

import codewar.utils as utils
from codewar.models import Problem
from codewar.enums import ProblemType, ProblemCategory


register = template.Library()
categorydict = {
    value: label
    for value, label in ProblemCategory.choices
}


@register.filter
def get_problem_content(problem, user):
    if user is None:
        return None
    is_forced = True if problem.solution_shown else utils.is_admin(user)
    return problem.get_content(utils.get_score(user), is_forced)


@register.filter
def get_button(problem, user):
    if user is None:
        return ''
    is_admin = utils.is_admin(user)
    attempts = problem.is_solved(user, is_admin)

    if problem.solution_shown:
        return (
            '<button class="btn btn-primary float-right" '
            'onclick="showSolution(' + str(problem.id) + ')"> '
            '<i class="icon-puzzle"></i> Show solution</button>'
        )
    is_solve_allowed = problem.is_solve_allowed(
        utils.get_score(user),
        is_admin
    )
    if not is_solve_allowed or attempts:
        return ''

    if problem.type == ProblemType.CODE or problem.type == ProblemType.AI:
        attr = f'href="#codechallenges/editor/{str(problem.id)}/"'
    else:
        attr = f'href="#" data-target="#problem{str(problem.id)}"'
    return (
        f'<a class="btn btn-primary float-right" data-toggle="modal" {attr}>'
        f'<i class="icon-fire"></i> Kill it and get {str(problem.score)}</a>'
    )


@register.filter
def get_user_score(user):
    return 0 if user is None else utils.get_score(user)


@register.filter
def get_event_prefix(prefix):
    return prefix or ""


@register.filter
def get_colored_title(problem, user):
    if not user:
        return "#F86C6B"
    if attempt := problem.is_solved(user, utils.is_admin(user)):
        return "#FF9633"
    if is_allowed := problem.is_solve_allowed(utils.get_score(user)):
        return "#20A8D8"


@register.filter
def get_category(value):
    return categorydict[value]


@register.filter
def show_score_admin(score, user):
    if user and utils.is_admin(user):
        return f'<b><i class="icon-trophy"></i>&nbsp;{str(score)} points</b>'
    return ''


@register.filter
def get_status(status):
    return utils.get_result_from_status(status)


@register.filter
def get_full_name(user_id):
    if not user_id:
        return None
    # This tag targets user only so that is_teamwork=False
    return utils.get_participant_fullname(user_id, False)
