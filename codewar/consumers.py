from channels.generic.websocket import AsyncJsonWebsocketConsumer
from codewar.tasks import start_compile
from asgiref.sync import sync_to_async

import logging


logger = logging.getLogger()


def logapi(consumer, event, content=None):
    contentmsg = f"[Content] {content}" if content else ""
    logger.info(
        f"<{consumer.scope['user']}--{consumer.channel_name}--{event}> "
        f"{contentmsg}"
    )


@sync_to_async
def _compile(user, data, channel):
    return start_compile(user, data, channel)


class CompilerTaskConsumer(AsyncJsonWebsocketConsumer):
    async def connect(self):
        self.room_group_name = 'CompilerTask'
        if self.scope['user'].is_anonymous:
            await self.close()
        else:
            await self.channel_layer.group_add(
                self.room_group_name,
                self.channel_name
            )
            await self.accept()
            logapi(self, 'connect')
            await self.send_json({
                'message': "Connected successfully",
                'success': True,
                'target': 'toast'
            })

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )
        logapi(self, 'disconnect')
        await self.close()

    async def receive_json(self, content):
        logapi(self, 'receive', content)
        if content.get('action') == 'compile_run':
            await _compile(
                self.scope['user'],
                content['data'],
                self.channel_name
            )

    async def send_json(self, content):
        logapi(self, 'send', content)
        await super().send_json(content)
