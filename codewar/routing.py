from django.urls import re_path

from codewar import consumers


websocket_urlpatterns = [
    # re_path(r'ws/chat/(?P<room_name>\w+)/$', consumers.CompilerTaskConsumer),
    re_path(r'^ws/tasks', consumers.CompilerTaskConsumer)
]
