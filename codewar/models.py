from django.contrib.auth.models import Group
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from django.conf import settings

from tinymce.models import HTMLField

from codewar.enums import (EventType, EventStatus,
    ProblemType, ProblemCategory, MarkedScore,
    AttemptLanguage, AttemptStatus)
import codewar.utils as utils


# Create your models here.
class Event(models.Model):
    name = models.CharField(
        max_length=64, blank=False, editable=True)
    prefix = models.CharField(
        max_length=64, blank=True, null=True, editable=True)
    time_start = models.DateTimeField()
    time_end = models.DateTimeField()
    type = models.IntegerField(
        choices=EventType.choices, default=EventType.CODE_CHALLENGE)
    # status should be property
    status = models.IntegerField(
        choices=EventStatus.choices, default=EventStatus.INACTIVE)
    is_teamwork = models.BooleanField(
        _('Teamwork'), default=False, editable=True)
    is_leaderboard = models.BooleanField(
        _('Leaderboard'), default=True, editable=True)
    description = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def get_time_start(self):
        return timezone.make_naive(
            self.time_start
        ).strftime('%d-%m-%Y %H:%M:%S %p')

    def get_time_end(self):
        return timezone.make_naive(
            self.time_end
        ).strftime('%d-%m-%Y %H:%M:%S %p')

    def get_current_status(self):
        now = timezone.now()
        if self.time_start <= now and self.time_end > now:
            return EventStatus.ACTIVE
        elif self.time_end <= now:
            return EventStatus.FINISHED
        else:
            return EventStatus.INACTIVE

    def get_prefix(self):
        return self.prefix or ''


class Problem(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        related_name='problem',
        blank=True,
        null=True,
        editable=False
    )
    event = models.ForeignKey(
        Event,
        on_delete=models.CASCADE,
        related_name='problem',
        null=True
    )
    type = models.IntegerField(
        choices=ProblemType.choices,
        default=ProblemType.CODE
    )
    title = models.CharField(max_length=255)
    score = models.IntegerField(default=0)
    required_score = models.IntegerField(default=0)
    category = models.IntegerField(
        choices=ProblemCategory.choices,
        default=ProblemCategory.CPP
    )
    published = models.BooleanField(default=False, editable=False)
    max_attempts = models.IntegerField(default=0)
    max_wrong = models.IntegerField(default=0)
    penalty_score = models.IntegerField(default=0)
    solution_shown = models.BooleanField(default=False, editable=False)
    content = HTMLField()
    solution = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
    binary_server = models.TextField(editable=True, blank=True, null=True)

    def __str__(self):
        return self.title

    def count_testcase(self):
        return self.testcase.count()

    def is_published(self, is_forced=False):
        return is_forced or self.is_published

    def _is_solve_allowed(self, score):
        return (self.required_score <= score and
            self.published and
            not self.solution_shown)

    def is_solve_allowed(self, score, is_forced=False):
        if is_forced or self._is_solve_allowed(score):
            return True
        return False

    def _is_solved(self, participant):
            return Attempt.objects.filter(
                event=self.event,
                problem=self,
                status=0,
                participant=utils.get_participant_id(
                    participant,
                    self.event.is_teamwork
                )
            )

    def is_solved(self, participant, is_forced=False):
        return None if is_forced else self._is_solved(participant)

    def list_solved(self):
        return Attempt.objects.filter(
            event=self.event,
            problem=self,
            status=AttemptStatus.FINISHED
        )

    def get_content(self, score, is_forced=False):
        if is_forced:
            return self.content

        if self.required_score > 0:
            return self.content if self.required_score <= score else \
                "Need more score to unlock it!"
        return self.content

    def get_real_score(self):
        count = Attempt.objects.filter(
            event=self.event,
            problem=self,
            status=AttemptStatus.FINISHED
        ).count()
        score = self.score - int(self.penalty_score) * count
        return self.penalty_score if score < self.penalty_score else score


class TestCase(models.Model):
    problem = models.ForeignKey(
        Problem,
        on_delete=models.CASCADE,
        related_name='testcase'
    )
    input = models.TextField(null=True, blank=True)
    output = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.input[:80]


class Attempt(models.Model):
    problem = models.ForeignKey(
        Problem,
        on_delete=models.CASCADE,
        related_name='attempt'
    )
    event = models.ForeignKey(
        Event,
        on_delete=models.CASCADE,
        related_name='attempt',
        null=True, editable=True
    )
    participant = models.IntegerField(
        null=True, blank=True, editable=False
    )
    content = models.TextField()
    language = models.CharField(
        max_length=8,
        choices=AttemptLanguage.choices,
        default=AttemptLanguage.CPP,
        null=True, blank=True
    )
    task = models.CharField(max_length=128, null=True, blank=True)
    status = models.IntegerField(
        choices=AttemptStatus.choices,
        null=True, blank=True
    )
    result = models.TextField(
        null=True, blank=True)
    marked_score = models.IntegerField(
        choices=MarkedScore.choices,
        default=MarkedScore.NO
    )
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.content[:80]

    def get_participant_name(self):
        return utils.get_participant(
            self.participant,
            self.event.is_teamwork
        )
    get_participant_name.short_description = 'Participant'


class Score(models.Model):
    participant = models.IntegerField(null=True, blank=True)
    event = models.ForeignKey(
        Event,
        on_delete=models.CASCADE,
        related_name='event'
    )
    score = models.IntegerField(null=True, blank=True, default=0)
    modified_at = models.DateTimeField("Last modify", auto_now=True)

    def __str__(self):
        return str(self.score)

    # TODO Change the function name
    def get_participant_name(self):
        return utils.get_participant(
            self.participant,
            self.event.is_teamwork
        )
    get_participant_name.short_description = "ID"

    def get_full_name(self):
        return utils.get_participant_fullname(
            self.participant,
            self.event.is_teamwork
        )
    get_full_name.short_description = "Name"

    def add_score(self, score):
        self.score += score


class GroupEvent(models.Model):
    event = models.ForeignKey(
        Event,
        on_delete=models.CASCADE,
        related_name='group_event'
    )
    group = models.ForeignKey(
        Group,
        on_delete=models.CASCADE,
        related_name='group_event'
    )
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def get_groupname(self):
        return utils.get_groupname_from_id(self.group.pk)
    get_groupname.short_description = 'Name'


class UserEvent(models.Model):
    event = models.ForeignKey(
        Event,
        on_delete=models.CASCADE,
        related_name='user_event'
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='user_event'
    )
    is_active = models.BooleanField(default=False)
    is_teamwork = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def get_username(self):
        return utils.get_user_from_id(self.user.pk)
    get_username.short_description = 'Name'
