from django.contrib.auth import get_user_model, authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.http import (
    HttpResponseRedirect, HttpResponseForbidden, JsonResponse)
from django.views.generic import RedirectView
from django.views.generic.base import TemplateView
from django.views.generic.detail import SingleObjectMixin, DetailView
from django.views.generic.list import ListView
from django.urls import reverse
from django.core.mail import EmailMultiAlternatives
import logging

from codewar.models import (Event, Problem, Attempt, Score, UserEvent)
from codewar.enums import (EventStatus, AttemptStatus, ProblemType)
import codewar.utils as utils


logger = logging.getLogger(__name__)


class IndexView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        has_joined_event = False
        is_leaderboard_visible = False
        if self.request.user.is_authenticated:
            if has_joined_event := UserEvent.objects.filter(
                    user=self.request.user,
                    is_active=True
                    ).all().count() == 1:
                event = utils.get_active_event(self.request.user)
                is_leaderboard_visible = event.is_leaderboard
        context['has_joined_event'] = has_joined_event
        context['is_leaderboard_visible'] = is_leaderboard_visible
        return context


class HomeView(LoginRequiredMixin, ListView):
    template_name = 'codewar/problems.html'
    context_object_name = 'problems'

    def get(self, request, *args, **kwargs):
        if utils.is_info_needed(request.user):
            return HttpResponseRedirect('codewar:me')
        if not utils.get_active_event(request.user):
            return HttpResponseForbidden()
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        return utils.get_problems(self.request.user)


class LoginView(TemplateView):
    template_name = 'login.html'

    def get(self, request, *args, **kwargs):
        user = request.user
        if user is None or not user.is_authenticated:
            return super().get(request, *args, **kwargs)
        if utils.is_info_needed(user):
            return ProfileView.as_view()(request, *args, **kwargs)
        else:
            return HomeView.as_view()(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        email = request.POST.get('email')
        password = request.POST.get('password')
        user = authenticate(
            request=request,
            email=email,
            password=password
        )
        if user is None:
            return JsonResponse({
                'type': 'auth',
                'success': False,
                'message': "Please check your username and password."
            })
        request.session.set_expiry(86400)
        login(request, user)
        return JsonResponse({
            'type': 'reload',
            'success': True,
        })


@login_required()
def user_logout(request):
    request.session.flush()
    logout(request)
    return HttpResponseRedirect(reverse('index'))


class HowToView(TemplateView):
    template_name = 'codewar/howto.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['hide_widget'] = True
        return context


class ProfileView(LoginRequiredMixin, DetailView):
    model = get_user_model()
    template_name = 'codewar/profile.html'

    def dispatch(self, request, *args, **kwargs):
        id_ = self.kwargs.get(self.pk_url_kwarg)
        if not id_:
            id_ = request.user.pk
            self.kwargs[self.pk_url_kwarg] = request.user.pk
        if not utils.is_admin(request.user) and id_ != request.user.pk:
            return HttpResponseForbidden()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        user = utils.get_user_from_id(request.user.pk)
        try:
            user.first_name = request.POST.get('first_name')
            user.last_name = request.POST.get('last_name')
            user.save()
            success = True
            message = "Updated successfully."
        except TypeError:
            success = False
            message = "Please double check your input."
        return JsonResponse({
            'type': 'toast',
            'success': success,
            'message': message
        })


class EventView(LoginRequiredMixin, ListView):
    model = Event
    queryset = Event.objects.filter(
        status=EventStatus.ACTIVE
    ).order_by('-time_start').all()
    template_name = 'codewar/event.html'
    context_object_name = 'events'

    def get(self, request, *args, **kwargs):
        if utils.is_info_needed(request.user):
            return HttpResponseRedirect(reverse('codewar:me'))
        return super().get(request, *args, **kwargs)


@login_required()
def show_solution(request, problem_id):
    if not (problem := Problem.objects.get(id=problem_id)):
        return JsonResponse({
            'type': 'toast',
            'success': False,
            'message': f"Can not find problem {problem_id}.",
        })
    if not problem.published or not problem.solution_shown:
        return JsonResponse({
            'type': 'toast',
            'success': False,
            'message': "This problem is hidden.",
        })
    attempts = problem.attempt.filter(
        status=AttemptStatus.FINISHED
    ).order_by(
        'created_at'
    ).values('participant', 'created_at', 'content')
    testcases = problem.testcase.get_queryset().values('input', 'output')
    return JsonResponse({
        'title': problem.title,
        'solution': problem.solution,
        'id': problem.id,
        'type': problem.type,
        'attempts': list(attempts),
        'testcases': list(testcases)
    })


class HistoryView(LoginRequiredMixin, ListView):
    template_name = 'codewar/history.html'
    model = Attempt
    context_object_name = 'attempts'

    def get_queryset(self):
        return utils.get_history(self.request.user)


class CodeEditorView(LoginRequiredMixin, DetailView):
    template_name = 'codewar/code-editor.html'
    model = Problem
    context_object_name = 'problem'

    def dispatch(self, request, *args, **kwargs):
        problem = self.get_object()
        if not problem.is_solve_allowed(
                utils.get_score(request.user),
                utils.is_admin(request.user)
                ):
            return HttpResponseRedirect(reverse('codewar:home'))
        return super().dispatch(request, *args, **kwargs)


@login_required()
def process_attempt(request, problem_id):
    if request.method != "POST":
        return

    problem = Problem.objects.get(id=problem_id)
    is_admin = utils.is_admin(request.user)
    event = utils.get_active_event(request.user, is_admin)

    if not problem.is_solve_allowed(
            utils.get_score(request.user), is_admin):
        return JsonResponse({
            'type': 'toast',
            'success': False,
            'message': "The problem is not available."
        })

    if event.get_current_status() != EventStatus.ACTIVE:
        return JsonResponse({
            'type': 'toast',
            'success': False,
            'message': "The event is ended already."
        })

    if problem.solution_shown:
        return JsonResponse({
            'type': 'toast',
            'success': False,
            'message': "The problem is over!"
        })

    attempt = (
        request.POST.getlist('answer', None)
        if problem.type == ProblemType.MULTIPLE_SELECT
        else request.POST.get('answer', None)
    )
    status, message = utils.check_attempt(problem, request.user, attempt)
    return JsonResponse({
        'type': 'toast',
        'success': status == AttemptStatus.FINISHED,
        'message': message
    })


class EventProcessView(LoginRequiredMixin, SingleObjectMixin, RedirectView):
    model = Event

    def get_redirect_url(self, *args, **kwargs):
        user = self.request.user
        if utils.is_info_needed(user):
            return HttpResponseRedirect(reverse('codewar:me'))
        event = self.get_object()
        is_teamwork = event.is_teamwork

        def _create_user_event(event, user):
            try:
                user_event, created = UserEvent.objects.get_or_create(
                    event=event,
                    user=user
                )
                user_event.is_active = True
                user_event.save()
                return reverse('codewar:home')
            except UserEvent.MultipleObjectsReturned:
                logger.warning(
                    "<user@{user}> has multiple events for <event@event.id>")
                return reverse('event')

        if is_teamwork and not utils.has_permission(user):
            return reverse('event')

        return _create_user_event(event, user)


class TopTenView(ListView):
    model = Score
    template_name = 'codewar/topten.html'
    context_object_name = 'scores'

    def get_queryset(self):
        event = utils.get_active_event(self.request.user)
        return Score.objects.filter(event=event).order_by('-score')

    def _get_attempts(self, **kwargs):
        user = self.request.user
        if not user.is_authenticated:
            return list()
        event = utils.get_active_event(user)
        if not (event
                and event.get_current_status() != EventStatus.INACTIVE
                and event.is_leaderboard):
            return list()
        user_score = dict()
        prob_score = dict()
        try:
            attempts = Attempt.objects.filter(
                problem__published=True,
                problem__event=event,
                status=AttemptStatus.FINISHED
            ).order_by('-created_at')
        except Attempt.DoesNotExist:
            return list()

        _attempts = list()
        for attempt in attempts:
            is_admin = (
                utils.get_groupname_from_id(
                    attempt.participant) == 'CodewarAdmin'
                if event.is_teamwork
                else utils.is_admin(
                    utils.get_user_from_id(attempt.participant))
            )
            if is_admin:
                continue

            if prob_score.get(attempt.problem) is None:
                prob_score[attempt.problem] = attempt.problem.score
            elif attempt.problem.penalty_score > 0:
                prob_score[attempt.problem] -= attempt.problem.penalty_score

            if prob_score.get(attempt.problem) < attempt.problem.penalty_score:
                prob_score[attempt.problem] = attempt.problem.penalty_score

            score = prob_score[attempt.problem]
            if user_score.get(attempt.participant) is None:
                user_score[attempt.participant] = score
            else:
                user_score[attempt.participant] += score

            _attempts.append({
                'title': attempt.problem.title,
                'participant': attempt.participant,
                'datetime': attempt.created_at,
                'score': user_score[attempt.participant],
            })
        return _attempts

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['attempts'] = self._get_attempts(**kwargs)
        return context


def feedback(request):
    if request.method != 'POST':
        return

    title = request.POST.get('title')
    content = request.POST.get('content')
    if not content:
        return JsonResponse({
            'type': 'toast',
            'success': False,
            'message': "Empty content.",
        })
    with open('codewar/templates/feedback.html') as f:
        html_template = f.read()
    email_from = request.POST.get('email')
    username = 'Anonymous' if email_from == 'false' else request.user.name
    html_content = html_template.format(
        title=title,
        username=username,
        content=content)
    subject = f"[CodingChallenge][FEEDBACK][{username}] {title}"
    email = EmailMultiAlternatives(
        subject, "",
        'mail.from@gmail.com',
        ['mail.to@gmail.com']
    )
    email.attach_alterative(html_content, 'text/html')
    email.send()
    return JsonResponse({
        'type': 'toast',
        'success': True,
        'message': "Thank you for your feedback.",
    })


@login_required()
def leave_event(request, pk):
    if event := utils.get_event_from_id(pk):
        utils.deactive_event(request.user, event)
    return HttpResponseRedirect(reverse('event'))


# TODO Change to FormView
class RegisterView(TemplateView):
    template_name = 'register.html'

    def post(self, request, *args, **kwargs):
        email = request.POST.get('email')
        password = request.POST.get('password')
        # password2 = request.POST.get('confirm_password')
        User = get_user_model()
        user = User.objects.filter(email=email)
        # TODO Add user form validation
        if user:
            pass
        user = User(email=email, is_active=True)
        user.set_password(password)
        user.save()
        return JsonResponse({
            'type': 'reload',
            'success': True,
            'message': "Account created."
        })


def forgot_password(request):
    pass


# TODO Implement these links
def verify_email(request, uidb64, token):
    pass


def confirm_password_reset(request, uidb64, token):
    pass
