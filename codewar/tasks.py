from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.conf import settings

from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync

import subprocess
from subprocess import PIPE, TimeoutExpired

import codewar.utils as utils
from codewar.models import Problem, Attempt
from codewar.enums import (EventStatus, 
    ProblemType, AttemptLanguage, AttemptStatus)


def send_message(channel, message):
    """Send a message to a websocket channel.
    """
    async_to_sync(
        get_channel_layer().send
    )(channel, message)


def msgapi(channel, msg=None, type='toast', error=None):
        send_message(channel, {
            'type': 'send_json',
            'target': type,
            'success': error is None,
            'message': error or msg
        })


compiledict = {
    AttemptLanguage.CPP: {
        'fn': '{root}/cpp_{filename}.cpp',
        'cmd': 'g++ -w -std=c++0x -Wall -o {filename}.out {filename}',
        'run': '{filename}.out',
    },
    AttemptLanguage.JAVA: {
        'fn': '{root}/java_{filename}.java',
        'sub': ('class CodeChallenges', 'class {filename}'),
        'cmd': 'javac {filename}',
        'run': 'java {filename}',
    },
    AttemptLanguage.NODEJS: {
        'fn': '{root}/nodejs_{filename}.js',
        'sub': ('class CodeChallenges', 'class {filename}'),
        'cmd': 'node --check {filename}',
        'run': 'node {filename}',
    }
}


def run_with_params(
        app_path,
        params,
        channel,
        language=AttemptLanguage.CPP):
    runcmd = compiledict[language]['run'].format(filename=app_path).split()
    app = subprocess.Popen(runcmd, stdin=PIPE, stdout=PIPE, stderr=PIPE)
    try:
        lines = bytes(params, 'utf-8')
        output, error = app.communicate(
            input=lines, timeout=settings.TEST_TIMEOUT)
        if error == b'':
            out = output.decode('utf-8')
            idx = out.find('\x00')
            return out if idx == -1 else out[:idx]
        return error.decode('utf-8')
    except TimeoutExpired:
        app.terminate()
        msgapi(channel, error="Time limit exceeded.", type='console')
        return None


def run_ai(server, built, channel, language=AttemptLanguage.CPP):
    pass


def compile_source(src, language):
    dict_ = compiledict.get(language)
    filename = dict_['fn'].format(
        root=settings.MEDIA_ROOT, 
        filename=utils.random_string()
    )
    file_ = default_storage.save(filename, ContentFile(src))
    command = dict_['cmd'].format(filename=filename).split()
    process = subprocess.Popen(command, stdout=PIPE, stderr=PIPE)
    return (file_, process.communicate())


def compile_and_test(
        id_, channel, participant, score,
        event, custom_input, language=AttemptLanguage.CPP):
    attempt = Attempt.objects.get(id=id_)
    file_, attempt.result = compile_source(attempt.content, language)

    if attempt.result != (b'', b''):
        attempt.status = AttemptStatus.COMPILE_FAILED
        attempt.save()
        msgapi(
            channel,
            error=attempt.result[1].decode('utf-8'),
            type='console'
        )
        return msgapi(error="Compile failed.", type='console')

    # AttemptStatus.COMPILE_SUCCESSFULLY
    attempt.status = AttemptStatus.COMPILE_SUCCESSFULLY
    msgapi(channel, msg="Compiled successfully.", type='console')
    if attempt.problem.type == ProblemType.AI:
        app_server = attempt.problem.binary_server
        output = run_ai(app_server, file_, channel, language)
        if not output or output == 'END':
            attempt.status = AttemptStatus.TEST_FAILED
            attempt.result = "Bot Failed"
            attempt.save()
            return msgapi(channel, error=attempt.result)
    elif attempt.problem.type == ProblemType.CODE:
        for index, testcase in enumerate(attempt.problem.testcase.all()):
            output = run_with_params(file_, testcase.input, channel, language)
            if output != testcase.output:
                msg_failed = (
                    f'Failed in test case: {str(index + 1)}'
                )
                msg_io = (
                    f'Input: {str(testcase.input)}\n'
                    f'Output: {str(output)}'
                )
                attempt.status = AttemptStatus.TEST_FAILED
                attempt.save()
                msgapi(channel, error=msg_failed)
                return msgapi(
                    channel,
                    error=f'{msg_failed}\n{msg_io}',
                    type='console'
                )
    attempt.result = 'Accepted'
    attempt.status = AttemptStatus.FINISHED
    attempt.save()
    utils.add_score(participant, event, score)
    return msgapi(channel, msg="This attempt is correct.")


def start_compile(user, data, channel):
    event = utils.get_active_event(user)
    problem = Problem.objects.get(id=int(data.get('problem_id')))

    if not problem:
        return msgapi(channel, error="Problem not found.")

    if problem.solution_shown:
        return msgapi(channel, error="This problem is ended.")

    if not data.get('editor'):
        return msgapi(channel, error="You have not coded.")

    if not user:
        return msgapi(channel, error="Please log in first.")

    if (event.get_current_status() != EventStatus.ACTIVE
             and not utils.is_admin(user)):
        return msgapi(channel, error="Event ended.")

    if utils.get_correct_attempt(problem, user):
        return msgapi(channel, error="You solved it already.")

    limited, message = utils.check_attempt_limit(problem, user)
    if limited:
        return msgapi(channel, error=message)

    pk = utils.get_participant_id(user, event.is_teamwork)
    langdict = {
        'cpp': AttemptLanguage.CPP,
        'java': AttemptLanguage.JAVA,
        'js': AttemptLanguage.NODEJS,
    }
    attempt = Attempt(participant=pk, event=event, content=data['editor'],
        problem=problem, language=langdict[data.get('language')])
    attempt.save()

    compile_and_test(
        attempt.id,
        channel,
        user,
        problem.get_real_score(),
        event,
        custom_input=data.get('custom_input'),
        language=langdict[data.get('language')])
    return msgapi(channel, msg="Your attempt has been added.")

    
# @task()
def generate_testcase(id_):
    """Generate output for testcases added to a problem,
    based on the solution given to it.
    """
    problem = Problem.objects.get(id=id_)
    file_, result = compile_source(problem.solution, AttemptLanguage.CPP)
    if result == (b'', b''):
        for testcase in problem.testcase.all():
            result = run_with_params(file_, testcase.input, None)
            testcase.output = result
            testcase.save()
