from django.db import models
from django.utils.translation import gettext as _


class EventType(models.IntegerChoices):
    CODE_CHALLENGE = 0, _('Code Challenge')
    CONTEST = 1, _('Contest')
    GUESS = 2, _('Guess')
    __empty__ = _('(Unknown)')


class EventStatus(models.IntegerChoices):
    FINISHED = 0, _('Finished')
    ACTIVE = 1, _('Active')
    INACTIVE = 2, _('Inactive')
    __empty__ = _('(Unknown)')


class ProblemType(models.IntegerChoices):
    CODE = (0, _('Code'))
    TEXT = (1, _('Text'))
    SELECT = (2, _('Select'))
    MULTIPLE_SELECT = (3, _('Multiple Select'))
    AI = (4, _('AI'))
    __empty__ = _('(Unknown)')


class ProblemCategory(models.IntegerChoices):
    CHALLENGE = (0, _('Code Challenge'))
    CPP = (1, _('C++'))
    GRAPHIC = (2, _('Graphic'))
    OPTIMIZATION = (3, _('Optimization'))
    __empty__ = _('(Unknown)')


class AttemptLanguage(models.TextChoices):
    CPP = ('.cpp', _('C++'))
    JAVA = ('.java', _('Java'))
    NODEJS = ('.js', _('NodeJS'))
    PYTHON = ('.py', _('Python'))


class AttemptStatus(models.IntegerChoices):
    FINISHED = (0, _('Finished'))
    COMPILE_SUCCESSFULLY = (1, _('Compiled successfully'))
    COMPILE_FAILED = (2, _('Compile failed'))
    TEST_FAILED = (3, _('Test failed'))
    WRONG = (4, _('Wrong'))
    MARKED = (5, _('Marked'))


class MarkedScore(models.IntegerChoices):
    NO = (0, _('No'))
    P25 = (1, _('25%'))
    P50 = (2, _('50%'))
    P75 = (3, _('75%'))
    P100 = (4, _('100%'))
