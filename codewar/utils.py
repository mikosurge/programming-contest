from django.contrib.auth import get_user_model
from django.utils.translation import gettext as _
from django.contrib.auth.models import Group

from collections import Counter
import json
import logging
import random
import string

import codewar.models as models
from codewar.enums import (EventType, EventStatus,
    AttemptStatus, ProblemType)


logger = logging.getLogger(__name__)
ASCII_AND_DIGITS = string.ascii_uppercase + string.digits
def random_char():
    """Randomize a character between [A-Z0-9].
    """
    return random.choice(ASCII_AND_DIGITS)


def random_string(length=10):
    """Randomize a string from randomized chars.
    """
    return ''.join(random_char() for _ in range(length))


def is_admin(user):
    if not user:
        return False
    group = Group.objects.get(name='CodewarAdmin')
    return group in user.groups.all() or user.is_superuser


def get_user_dirpath(instance, filename):
    """Generate appropriate user directory path.
    """
    if hasattr(instance, 'user'):
        return f'{instance.user.username}/{random_string()}_{filename}'
    return f'{random_string()}_{filename}'


def get_user_from_id(userid):
    User = get_user_model()
    try:
        return User.objects.get(pk=userid)
    except User.DoesNotExist:
        return None


def get_group_from_id(groupid):
    try:
        return Group.objects.get(pk=groupid)
    except Group.DoesNotExist:
        return None


def get_group_name_from_id(groupid):
    if group := get_group_from_id(groupid):
        return group.name
    return None


def get_event_from_id(event_id):
    try:
        event = models.Event.objects.get(pk=event_id)
    except models.Event.DoesNotExist:
        event = None
    return event


def get_user_from_username(username):
    User = get_user_model()
    try:
        return User.objects.get(username=username)
    except User.DoesNotExist:
        return None


def get_group_from_userid(userid):
    try:
        user = get_user_from_id(userid)
        return user.groups.get()
    except Group.DoesNotExist:
        return None


def get_users_from_groupname(groupname):
    try:
        group = Group.objects.get(name=groupname)
        return group.user_set.all()
    except Group.DoesNotExist:
        return None


def get_participant(id_, is_teamwork):
    """Retrieve either user or group name that joined the event.
    """
    return (
        get_groupname_from_id(id_) if is_teamwork
        else get_user_from_id(id_)
    )


def get_participant_id(userobj, is_teamwork=None):
    """Return the groupid if the event is a teamwork event,
    or userid otherwise.
    """
    if is_teamwork is None:
        event = get_active_event(userobj)
        is_teamwork = event.is_teamwork

    return (
        get_group_from_userid(userobj.pk).pk if is_teamwork
        else userobj.pk
    )


def get_participant_fullname(id_, is_teamwork):
    """Retrieve the full name of the user of group that joined the event.
    """
    participant = get_participant(id_, is_teamwork)
    return (
        participant if is_teamwork
        else participant.get_full_name() if participant else "-"
    )


def get_active_event(user, is_forced=False):
    """Retrieve the currently active event of a user.
    """
    try:
        user_event = models.UserEvent.objects.get(user=user, is_active=True)
        event = user_event.event
        return event
    except models.UserEvent.DoesNotExist:
        logger.warning(f"UserEvent of <user@{user}> does not exist.")
    except models.UserEvent.MultipleObjectsReturned:
        logger.warning(f"Multiple UserEvent of <user@{user}> returned.")
    return None


def get_problems(user):
    if not (event := get_active_event(user)):
        return Problem.objects.none()
    problems = models.Problem.objects.filter(event=event)
    event_status = event.get_current_status()
    if event_status == EventStatus.ACTIVE or is_admin(user):
        return problems.filter(published=True)
    elif event_status == EventStatus.FINISHED:
        return problems.filter(solution_shown=True)
    return problems


def get_history(user):
    return list(models.Attempt.objects.filter(
        participant=get_participant_id(user),
        problem__published=True
    ).order_by('-created_at'))


def get_score(user):
    event = get_active_event(user)
    if event and event.get_current_status() == EventStatus.ACTIVE:
        try:
            pk = get_participant_id(user)
            score = models.Score.objects.filter(
                participant=pk,
                event=event
            ).get()
            return score.score
        except models.Score.DoesNotExist:
            pass
    return 0


def add_score(user, event, score_to_add=0):
    if not is_admin(user) and event:
        pk = get_participant_id(user, event.is_teamwork)
        try:
            score = models.Score.objects.filter(
                participant=pk,
                event=event
            ).get()
            score.add_score(score_to_add)
        except models.Score.DoesNotExist:
            score = models.Score(
                participant=pk,
                event=event,
                score=score_to_add
            )
        finally:
            score.save()
            return True
    return False


def is_info_needed(user):
    return not user.first_name or not user.last_name


def get_correct_attempt(problem, user):
    """Return the correct attempt for a problem of a user or nothing.
    """
    event = get_active_event(user)
    if event:
        return models.Attempt.objects.filter(
            event=event,
            participant=get_participant_id(user, event.is_teamwork),
            problem=problem,
            status=AttemptStatus.FINISHED
        ).first()
    return None


def has_permission(event, user):
    if is_admin(user):
        return True

    group_events = models.GroupEvent.objects.filter(event=event)
    for group_event in group_events:
        users = get_users_from_group(group_event.group)
        if user in users:
            return True
    return False


def deactive_event(user, event=None):
    if not user:
        return False

    def _single(user_event):
        user_event.is_active = False
        user_event.save()
        return True

    if event:
        try:
            return _single(models.UserEvent.objects.get(event=event, user=user))
        except models.UserEvent.DoesNotExist:
            logger.warning(
                f"UserEvent of <user@{user}> and <event@{event}>"
                "does not exist."
            )
            return False
    else:
        [
            _single(user_event)
            for user_event in models.UserEvent.objects.filter(user=user).all()
        ]
        return True


def handle_attempt(is_correct, user, problem, input_):
    event = get_active_event(user)
    if event.get_current_status() != EventStatus.ACTIVE and not is_admin(user):
        return None

    attempt_status = AttemptStatus.WRONG
    if event.type == EventType.CODE_CHALLENGE:
        if is_correct:
            attempt_status = AttemptStatus.FINISHED
            message = _("This answer is correct!")
            if not get_correct_attempt(problem, user):
                add_score(user, event, problem.get_real_score())
        else:
            message = _("This answer is NOT correct!")
    elif event.type == EventType.GUESS:
        attempt_status = AttemptStatus.FINISHED
        message = _("Your answer has been received.")

    if problem.published and not is_admin(user):
        models.Attempt(
            participant=get_participant_id(user),
            event=event,
            content=input_,
            problem=problem,
            result=get_result_from_status(attempt_status),
            status=attempt_status
        ).save()
    return attempt_status, message


def check_attempt_limit(problem, user):
    """Return a tuple containing a boolean showing if limit reached
    and its corresponding message.
    """
    num_wrong = models.Attempt.objects.filter(
        participant=get_participant_id(user),
        problem=problem,
        status__gt=AttemptStatus.FINISHED
    ).count()
    total_correct = models.Attempt.objects.filter(
        problem=problem,
        status=AttemptStatus.FINISHED
    ).count()
    if problem.max_attempts > 0 and total_correct >= problem.max_attempts:
        return (
            True,
            _(f"Slow hand! We've got {problem.max_attempts} answers already.")
        )
    elif problem.max_wrong > 0 and num_wrong >= problem.max_wrong:
        return (
            True, _(f"Wrong attempt limit of {problem.max_wrong} reached.")
        )
    return False, ""


def check_attempt(problem, user, input_):
    if get_correct_attempt(problem, user):
        return (
            AttemptStatus.FINISHED,
            _("You have already submitted the correct answer!")
        )

    if not input_ or not problem:
        return AttemptStatus.WRONG, _("Please enter a solution!")

    limited, message = check_attempt_limit(problem, user)
    if limited:
        return AttemptStatus.WRONG, message

    if problem.type == ProblemType.MULTIPLE_SELECT:
        correct_choices = list(problem.testcase.filter(
            problem=problem,
            output__icontains='true'
        ))
        user_choices = list(problem.testcase.filter(id__in=input_))
        is_correct = Counter(correct_choices) == Counter(user_choices)
        content = '\n'.join([choice.input for choice in user_choices])
    elif problem.type == ProblemType.SELECT:
        correct_choice = problem.testcase.filter(output__icontains='true')
        user_choice = problem.testcase.filter(id=input_)
        is_correct = Counter(correct_choice) == Counter(user_choice)
        content = user_choice.first().input
    elif problem.type == ProblemType.TEXT:
        def condense(str_):
            return str_.lower().replace(' ', '')
        is_correct = any(
            condense(testcase.input) == condense(input_)
            for testcase in problem.testcase.all()
        )
        content = input_
    else:  # ProblemType.CODE:
        is_correct = False
        content = input_
    return handle_attempt(is_correct, user, problem, content)


def send_email(email, username, type_, url):
    pass


def get_domain():
    # TODO Add a real host here
    return (
        "http://localhost:8000" if settings.DEBUG
        else "http://localhost:8000"
    )


def get_result_from_status(status):
    return "Accepted" if status == 0 else "Wrong"


def get_statistic(event_id):
    stats = list()
    if event_id:
        event = get_event_from_id(event_id)
        user_events = models.UserEvent.objects.filter(
            event=event
        )
        for user_event in user_events:
            stat = dict()
            stat['fullname'] = user_event.user.get_full_name()
            stat['event'] = event.name
            stat['email'] = user_event.user.email
            try:
                score = modelsScore.objects.get(
                    event=event,
                    user=user_event.user.pk
                )
                stat['score'] = score.score
            except models.Score.DoesNotExist:
                stat['score'] = 0
            stats.append(stat)
    return stats
