from django.test import TestCase
from django.contrib.auth import get_user_model

from core import models


DEFAULT_EMAIL = 'test@springs.com'
DEFAULT_PASSWORD = 'test123'
def sample_user(email=DEFAULT_EMAIL, password=DEFAULT_PASSWORD):
    """Create a sample user from given default username and password.
    """
    return get_user_model().objects.create_user(email, password)


class ModelTests(TestCase):
    def test_create_user(self):
        """Test creating a new user with an email.
        """
        User = get_user_model()
        user = User.objects.create_user(
            email='normal@springs.com',
            password='normal123'
        )
        self.assertEqual(user.email, 'normal@springs.com')
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)
        try:
            self.assertIsNone(user.username)
        except AttributeError:
            pass
        with self.assertRaises(TypeError):
            User.objects.create_user()
        with self.assertRaises(ValueError):
            User.objects.create_user(email='')
        with self.assertRaises(ValueError):
            User.objects.create_user(email='', password='foo')

    def test_create_superuser(self):
        """Test creating a new superuser.
        """
        User = get_user_model()
        admin_user = User.objects.create_superuser(
            email='super@springs.com',
            password='super123'
        )
        self.assertEqual(admin_user.email, 'super@springs.com')
        self.assertTrue(admin_user.is_active)
        self.assertTrue(admin_user.is_staff)
        self.assertTrue(admin_user.is_superuser)
        try:
            self.assertIsNone(admin_user.username)
        except AttributeError:
            pass

    def test_create_user_email_normalized(self):
        """Test the email for a new user is normalized.
        """
        email = 'test@SPRINGS.COM'
        user = get_user_model().objects.create_user(email, 'test123')
        self.assertEqual(user.email, email.lower())
