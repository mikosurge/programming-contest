'use strict'


// Active ajax page loader
$.ajaxLoad = true;

// Required when $.ajaxLoad = true
$.defaultPage = 'event';
$.subPagesDirectory = '/';
$.page404 = 'views/page/404.html';
$.mainContent = $('#ui-view');


// $.panelIconOpened = 'icon-arrow-up';
// $.panelIconClosed = 'icon-arrow-down';

//Default colours
$.brandPrimary = '#20a8d8';
$.brandSuccess = '#4dbd74';
$.brandInfo = '#63c2de';
$.brandWarning = '#f8cb00';
$.brandDanger = '#f86c6b';


const timeToString = time => {
  // Warning: Error-prone, due to +7
  const [yyyy, MM, dd, HH, mm, ss] = time.split(/-|T|Z|:| /);
  return `${dd}/${MM}/${yyyy} ${Number(HH) + 7}:${mm}:${ss}`;
};


const pad = n => (n < 10 ? `0${n}` : n.toString());
const isJsonString = str => {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
};


const getCountdown = (date, target) => {
  var hh, mm, ss;
  var currentDate = (new Date().getTime()) / 1000;
  var secondsLeft = date - currentDate;
  if (secondsLeft >= 0){
    hh = pad(parseInt(secondsLeft / 3600));
    secondsLeft = secondsLeft % 3600;
    mm = pad(parseInt(secondsLeft / 60));
    ss = pad(parseInt(secondsLeft % 60));
    target.innerHTML = `<span>${hh} : ${mm} : ${ss}</span>`;
  } else {
    window.location.reload(true);
  }
};


const isFormUrl = url => (
  url &&
  url.length > 4 &&
  url.substr(0, 4) === 'form'
);


const reloadTopTen = () => {
  $('#topten').load(
    '/codechallenges/topten', null, responseText => {
    }
  ).delay(250).animate({
    opacity: 1
  }, 0);
};


const submitAjax = formInput => {
  const formData = new FormData(formInput[0]);
  $.ajax({
    url: formInput.attr('action'),
    type: "POST",
    data: formData,
    cache: false,
    async: true,
    contentType: false,
    processData: false,
    success: function (res) {
      const { type, success, message } = res;
      if (type === 'toast') {
        if (success) {
          toastr.success(message);
        } else {
          toastr.error(message);
        }
      } else if (type === 'auth' && success === false) {
        $('#text-msg').html(message);
      } else if (type === 'reload' && success === true) {
        location.reload();
      }
    },
    error: function (xhr, errmsg, err) {
      toastr.error(err);
    }
  });
};


const loadPage = url => {
  if (!url) return;

  if (isFormUrl(url)) {
    submitAjax($(`#${url}`));
  } else {
    if (window.location.hash.includes('/editor/')) {
      // TODO Change the variable name
      if (typeof autoSave !== 'undefined') {
        clearInterval(autoSave);
      }
    } else if (window.location.hash.includes('#event')) {
      if (typeof eventList !== 'undefined' && eventList.length) {
        eventList.forEach(
          element => { clearInterval(element.id); }
        );
      }
    }
    let stateObj = {
      url: window.location.hash,
      innerHTML: document.getElementById('ui-view').innerHTML,
    };
    window.history.pushState(stateObj, null, null);

    if (url.match(RegExp('^event\/[0-9]\/leave$'))){
      window.location.hash = 'event';
      window.location.reload(true);
    }

    if (url.match(RegExp('^event\/[0-9]$'))){
      window.location.hash = 'codechallenges/';
      window.location.reload(true);
    }

    Pace.restart();

    $('html, body').animate({
      scrollTop: 0
    }, 0);

    $.mainContent.load(`${$.subPagesDirectory}${url}`, null, function (responseText, status, jqXHR) {
      if (jqXHR.status == 403) {
        toastr.error("Permission denied");
      }
      window.location.hash = url;
    }).delay(250).animate({
      opacity: 1
    }, 0);

    reloadTopTen();
  }
};


const setUpUrl = url => {
  url = url.replace('/#', '');
  url = url.replace('#', '');
  $('nav .nav li .nav-link').removeClass('active');
  $('nav .nav li.nav-item').removeClass('open');
  $(`nav .nav li:has(a[href="#${url.split('?')[0]}"])`).addClass('open');
  $(`nav .nav a[href="#${url.split('?')[0]}"]`).addClass('active');
  loadPage(url);
};


const resizeBroadcast = () => {
  let timesRun = 0;
  const interval = setInterval(() => {
    timesRun += 1;
    if (timesRun === 5) {
      clearInterval(interval);
    }
    window.dispatchEvent(new Event('resize'));
  }, 62.5);
}


const init = url => {
  // Tooltip
  $('[rel="tooltip"],[data-rel="tooltip"]').tooltip({
    "placement": "bottom",
    delay: {
      show: 400,
      hide: 200
    }
  });

  // Popover
  $('[rel="popover"],[data-rel="popover"],[data-toggle="popover"]').popover();
}


const setCookie = (cname, cvalue, exdays) => {
  var d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  const expires = `expires=${d.toGMTString()}`;
  document.cookie = `${cname}=${escape(cvalue)};${expires};path=/`;
};


const getCookie = name => {
  const re = new RegExp(name + "=([^;]+)");
  const value = re.exec(document.cookie);
  return value ? unescape(value[1]) : null;
};


if ($.ajaxLoad) {
  // let paceOptions = {
  //   elements: false,
  //   restartOnRequestAfter: false
  // };

  const url = location.hash.replace(/^#/, '');
  toastr.options = {
    "positionClass": "toast-bottom-right",
    "closeButton": false,
    "debug": false,
    "newestOnTop": true,
    "progressBar": true,
    "preventDuplicates": false,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  };

  setUpUrl(url || $.defaultPage);
}


window.onpopstate = function (event) {
  var currentState = history.state;
  if (currentState && currentState.innerHTML) {
    document.getElementById("ui-view").innerHTML = currentState.innerHTML;
  }
};


$(document).on('click', 'a[href!="#"]', function (e) {
  e.preventDefault();
  const target = $(e.currentTarget);
  if ($(this).attr('target') == '_top') {
    window.location = (target.attr('href'));
  } else if ($(this).attr('target') == '_blank') {
    window.open(target.attr('href'));
  } else {
    if (!($(this).parent().parent().hasClass('nav-tabs') ||
         $(this).parent().parent().hasClass('nav-pills') ||
         $(this).attr('target') === '_none')) {
      setUpUrl(target.attr('href'));
    }
  }
});


$(document).on('click', 'a[href="#"]', function (e) {
  e.preventDefault();
});


$(document).ready(function ($) {
  // Dropdown Menu
  $('nav > ul.nav').on('click', 'a', e => {
    if ($.ajaxLoad) {
      e.preventDefault();
    }

    if ($(this).hasClass('nav-dropdown-toggle')) {
      $(this).parent().toggleClass('open');
      resizeBroadcast();
    }
  });

  reloadTopTen();

  // Main Menu
  $('.sidebar-toggler').click(() => {
    $('body').toggleClass('sidebar-hidden');
    resizeBroadcast();
  });

  $('.sidebar-minimizer').click(() => {
    $('body').toggleClass('sidebar-minimized');
    resizeBroadcast();
  });

  $('.brand-minimizer').click(() => {
    $('body').toggleClass('brand-minimized');
  });

  $('.aside-menu-toggler').click(() => {
    $('body').toggleClass('aside-menu-hidden');
    resizeBroadcast();
    if (!$('body.aside-menu-hidden')[0]) {
        reloadTopTen();
    }
  });

  $('.mobi-aside-menu-toggler').click(() => {
    $('body').toggleClass('aside-menu-show');
    resizeBroadcast();
    if (!$('body.aside-menu-show')[0]) {
      reloadTopTen();
    }
  });

  $('.mobile-sidebar-toggler').click(() => {
    $('body').toggleClass('sidebar-mobile-show');
    resizeBroadcast();
  });

  $('.sidebar-close').click(() => {
      $('body').toggleClass('sidebar-opened').parent().toggleClass('sidebar-opened');
  });

  // Disable jumping to top
  $('a[href="#"][data-top!=true]').click(e => {
      e.preventDefault();
  });
});



const solutionEditor = ace.edit('solution');
solutionEditor.setTheme('ace/theme/sqlserver');
solutionEditor.session.setMode('ace/mode/c_cpp');
solutionEditor.setReadOnly(true);
solutionEditor.session.setUseWrapMode(true);


const _createTestcaseContent = testcases => {
  const content = testcases.map(
    testcase => (`
      <tr>
        <td>${testcase.input}</td>
        <td>${testcase.output}</td>
      </tr>
    `)
  ).join('\n');
  return `
    <table class="table table-bordered table-striped table-sm">
      <thead><tr><th>Input</th><th>Output</th></tr></thead>
      <tbody>
        ${content}
      </tbody>
    </table>
  `
};


const _createUserTable = attempts => {
  const content = attempts.map(
    attempt => (`
      <tr>
        <td>${attempt.participant}</td>
        <td>${timeToString(attempt.created_at)}</td>
      </tr>
    `)
  ).join('\n');
  return `
    <div><a href="#" data-toggle="modal" data-target="#feedback">Report</a></div>
    <table class="table table-bordered table-striped table-sm">
      <thead><tr><th>Input</th><th>Output</th></tr></thead>
      <tbody>
        ${content}
      </tbody>
    </table>
  `
};


const editSolution = solution => {
  solutionEditor.setValue(solution);
  solutionEditor.gotoLine(0);
}


const _showSolution = data => {
  $("#modalTitle").text(data.title);

  // Create the test case table
  $("#testcaseTab").html(_createTestcaseContent(data.testcases));

  // User atttempts
  $("#userAttemptTab").html(_createUserTable(data.attempts));

  $(".solution").click(event => {
    event.preventDefault();
    event.stopPropagation();
    const solutionWindow = window.open(this.getAttribute("href"), "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=0,left=0,width=600,height=600,directories=no,location=no");
    solutionWindow.focus();
  });

  $("#showSolutionModal").on('shown.bs.modal', editSolution(data.solution));
  $("#showSolutionModal").modal('show');
  $("#tabSolution").tab('show');
};


const showSolution = problemId => {
  $.ajax({
    url: `codechallenges/showsolution/${problemId}`,
    success: data => {
      _showSolution(data);
    }
  });
};
