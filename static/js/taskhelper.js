//Init Toastr
toastr.options = {
    "positionClass": "toast-bottom-right",
    "closeButton": false,
    "debug": false,
    "newestOnTop": true,
    "progressBar": true,
    "preventDuplicates": false,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

// Use wss/ws corresponding to the http protocol
const isSecuredProtocol = window.location.protocol === 'https:';
const isLocalhost = window.location.host.includes('localhost');

const wsScheme = isSecuredProtocol ? 'wss' : 'ws';
const wsPort = isSecuredProtocol ? '443' : '80';
const wsSuffix = isLocalhost ? '' : `:${wsPort}`;
const wsPath = `${wsScheme}://${window.location.host}${wsSuffix}/ws/tasks`;
const socket = new ReconnectingWebSocket(wsPath);

socket.onmessage = message => {
  const data = JSON.parse(message.data);
  const outputdiv = document.getElementById('iframeResult');
  if (data.target === "console" && data.message && outputdiv) {
    outputdiv.innerHTML = `
      ${outputdiv.innerHTML}
      <p>
        ${data.message.replace(/(?:\r\n|\r|\n)/g, '<br />')}
      </p>
    `;
  } else if (data.target === "toast" && data.message) {
    if (data.success) {
      toastr.success(data.message);
    } else {
      toastr.error(data.message);
    }
  }

  if (data.reloadTopTen) {
    $('#topten').load(
      '/codechallenges/topten', null, responseText => {}
    ).delay(250).animate({
      opacity: 1
    }, 0);
  }
};

socket.onclose = e => {
  toastr.options = {
    "positionClass": "toast-bottom-right"
  };
};


const compileAndRun = e => {
  const outputdiv = document.getElementById('iframeResult');
  outputdiv.innerHTML = "";
  if (e.getAttribute('action') !== 'compile_run') {
    return;
  }
  message = {
    action: 'compile_run',
    data: {
      'editor': editor.getValue(),
      'problem_id': $('#problem_id').val(),
      'custom_input': $('#cinput').val(),
      'language': $('#language').val()
    }
  };
  toastr.info("Sent to Server");
  socket.send(JSON.stringify(message));
};
